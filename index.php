<?php
require __DIR__ . '/vendor/autoload.php';
use App\Entity\User;

$hero = new \App\Entity\Player();
$hero->initialisePlayer(true);
$beast = new \App\Entity\Player();
$beast->initialisePlayer();

$battle = new \App\Controller\BattleController();
echo $battle->fight($hero,$beast);

