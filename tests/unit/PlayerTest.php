<?php

class PlayerTest extends \PHPUnit\Framework\TestCase{

    public function testRandomStartIfEqualSpeedAndLuck(){

        $battle = new \App\Controller\BattleController();
        $beast = new \App\Entity\Player();
        $beast->initialisePlayer();

        $hero = new \App\Entity\Player();
        $hero->initialisePlayer(true);

        $beast->setSpeed(80);
        $hero->setSpeed(80);

        $beast->setLuck(25);
        $hero->setLuck(25);

        $battle->fight($hero, $beast);

        $this->assertEquals('random', $battle->getPlayer1()->getPriorityAssignedBy());
    }

    public function testPriorityByLuck(){

        $battle = new \App\Controller\BattleController();
        $beast = new \App\Entity\Player();
        $beast->initialisePlayer();

        $hero = new \App\Entity\Player();
        $hero->initialisePlayer(true);

        $beast->setSpeed(80);
        $hero->setSpeed(80);

        $beast->setLuck(27);
        $hero->setLuck(25);

        $battle->fight($hero, $beast);

        $this->assertEquals('luck', $beast->getPriorityAssignedBy());
    }

    public function testPriorityBySpeed(){

        $battle = new \App\Controller\BattleController();
        $beast = new \App\Entity\Player();
        $beast->initialisePlayer();

        $hero = new \App\Entity\Player();
        $hero->initialisePlayer(true);

        $beast->setSpeed(80);
        $hero->setSpeed(81);

        $beast->setLuck(27);
        $hero->setLuck(25);

        $battle->fight($hero, $beast);

        $this->assertEquals('speed', $hero->getPriorityAssignedBy());
    }

    public function testIfGameEndsAfter20Rounds(){

        $battle = new \App\Controller\BattleController();
        $beast = new \App\Entity\Player();
        $beast->initialisePlayer();

        $hero = new \App\Entity\Player();
        $hero->initialisePlayer(true);

        //setting minimum damage
        $beast->setStrength(80);
        $hero->setStrength(80);

        $beast->setDefence(79);
        $hero->setDefence(79);


        $battle->fight($hero, $beast);

        $this->assertEquals('20', $battle->getRound());
    }

}