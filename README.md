# Hero H1

Guide our hero through the forests of Emagia!

## Setup

If you've just downloaded the code, congratulations!!

To get it working, pour some coffee or tea, and
follow these steps:

**Download Composer dependencies**

Make sure you have [Composer installed](https://getcomposer.org/download/)
and then run:

```
composer install
```

You may alternatively need to run `php composer.phar install`, depending
on how you installed Composer.



**Start the game**

Simply go to project root

Have fun!

## Have Ideas, Feedback or an Issue?

If you have suggestions or questions, please feel free to
open an issue on this repository. We're it! :).

## Thanks!

And as always, thanks so much for your support and letting
us do what we love!

<3 Your friends at eMag
