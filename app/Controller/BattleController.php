<?php

namespace App\Controller;

use App\Entity\Player;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFilter;

class BattleController
{

    /** @var Player */
    public $player1;
    /** @var Player */
    public $player2;
    /** @var Player */
    private $hero;
    /** @var Player */
    private $beast;

    /** @var Environment */
    public $twig;

    private $log = array();

    public $gameActive = false;

    public $round;

    public function fight($hero, $beast)
    {
        $this->startGame();
        $this->hero = $hero;
        $this->beast = $beast;
        $this->resolvePlayerOrder();

        $loader = new FilesystemLoader(__DIR__ . '/../');
        $twig = new Environment($loader, [
            //'cache' => __DIR__ . '/cache'
        ]);


        $round = 0;
        while ($round < 20) {
            $round++;
            $this->setRound($round);

            if ($this->player1->attack($this->player2, $this, $round)) {
                break;
            }
            if ($this->player2->attack($this->player1, $this, $round)) {
                break;
            }

        }

        if ($this->isGameActive()) {
            if ($this->player1->getHealth() == $this->player2->getHealth()) {
                $this->addLog('Nobody WON! It\'s a draw', $round);
            } else {
                $winnerName = $this->player1->getHealth() > $this->player2->getHealth() ?
                    $this->player1->playerName : $this->player2->playerName;
                $this->addLog($winnerName . ' WON!', $round);
            }
        }


        return $twig->render('templates/log.html.twig',
            [
                'log' => $this->getLog()

            ]
        );


    }


    private function resolvePlayerOrder()
    {


        if ($this->hero->getSpeed() == $this->beast->getSpeed()) {
            if ($this->hero->getLuck() == $this->beast->getLuck()) {
                //assign priority randomly
                $this->player1 = (bool)rand(0, 1) ? $this->beast : $this->hero;
                $this->player1->setPriorityAssignedBy('random');

            } else {
                //assign the player with highest luck
                $this->player1 = ($this->hero->getLuck() > $this->beast->getLuck()) ? $this->hero : $this->beast;
                $this->player1->setPriorityAssignedBy('luck');
            }
        } else {
            //assign the player with highest speed
            $this->player1 = ($this->hero->getSpeed() > $this->beast->getSpeed()) ? $this->hero : $this->beast;
            $this->player1->setPriorityAssignedBy('speed');
        }
        //set the second player
        $this->player2 = $this->player1->isHero() ? $this->beast : $this->hero;
    }

    /**
     * @return array()
     */
    public function getLog()
    {
        return $this->log;
    }


    public function addLog($log, $round = 0)
    {
        $this->log[$round][] = $log;
    }

    public function startGame()
    {
        $this->gameActive = true;
    }

    public function endGame()
    {
        $this->gameActive = false;
    }

    public function isGameActive()
    {
        return $this->gameActive;
    }

    /**
     * @return Player
     */
    public function getPlayer1()
    {
        return $this->player1;
    }

    /**
     * @param Player $player1
     */
    public function setPlayer1($player1)
    {
        $this->player1 = $player1;
    }

    /**
     * @return Player
     */
    public function getPlayer2()
    {
        return $this->player2;
    }

    /**
     * @param Player $player2
     */
    public function setPlayer2($player2)
    {
        $this->player2 = $player2;
    }

    /**
     * @return mixed
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * @param mixed $round
     */
    public function setRound($round)
    {
        $this->round = $round;
    }




}