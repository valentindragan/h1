<?php

namespace App\Entity;

use App\Controller\BattleController;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Player
{

    private $health;
    private $strength;
    private $defence;
    private $speed;
    private $luck;
    private $hero;
    public $playerName;
    public $priorityAssignedBy;




    /**
     * @param false $hero
     * @return void
     */
    public function initialisePlayer($hero = false)
    {

        if ($hero) {
            $this->initialiseHero();
        } else {
            $this->initialiseBeast();
        }

    }


    /**
     * Sets defauts for Orderus
     * @return void
     */
    private function initialiseHero()
    {

        $this->setHealth(rand(70, 100));
        $this->setStrength(rand(70, 80));
        $this->setDefence(rand(45, 55));
        $this->setSpeed(rand(40, 50));
        $this->setLuck(rand(10, 30));
        $this->setHero();

    }

    /**
     * Sets defaults for the beast
     * @return void
     */
    private function initialiseBeast()
    {

        $this->setHealth(rand(60, 90));
        $this->setStrength(rand(60, 90));
        $this->setDefence(rand(40, 60));
        $this->setSpeed(rand(40, 60));
        $this->setLuck(rand(25, 40));

    }

    /**
     * Last attack returns true in order to end game
     * @param Player $rival
     * @param BattleController $battle
     * @param $round
     * @param false $secondAttack
     * @return bool
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function attack(Player $rival, BattleController $battle, $round, $secondAttack = false)
    {
        $this->playerName = $this->isHero() ? 'Orderus' : 'Beast';
        $this->rivalName = !$this->isHero() ? 'Orderus' : 'Beast';
        $loader = new FilesystemLoader(__DIR__ . '/../');
        $twig = new Environment($loader, [

        ]);

        $stats = $twig->render('templates/stats.html.twig',
            [
                'playerName' => $this->playerName,
                'player' => $this,
                'rival' => $rival,
                'rivalName' => $this->rivalName

            ]
        );
        $battle->addLog($stats, $round);
        $battle->addLog($this->playerName . ' attacks!', $round);



        $damage = $this->getStrength() - $rival->getDefence();
        $battle->addLog('Potential damage of ' . $damage . '.', $round);

        if ($rival->hasMagicShield()) {
            $damage = $damage / 2;
            $battle->addLog($this->rivalName . ' got the magic shield. Potential damage decreased to ' . $damage, $round);
        }

        if ($rival->isLucky()) {
            $battle->addLog($this->rivalName . ' got lucky and blocked the attack', $round);
        } else {
            $rival->setHealth($rival->getHealth() - $damage);
            $battle->addLog($this->rivalName . ' has been damaged by ' . $damage . '%', $round);
        }

        if ($rival->getHealth() <= 0) {
            $battle->addLog($this->playerName . ' WON!', $round);
            $battle->endGame();
            return true;
        }

        if ($this->isHero() && $this->hasRapidStrike() && !$secondAttack) {
            $battle->addLog('Rapid Strike', $round);
            return $this->attack($rival, $battle, true);
        }
        return false;
    }

    /**
     * @return bool
     */
    private function isLucky()
    {
        if (rand(1, 100) <= $this->getLuck()) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    private function hasRapidStrike()
    {
        if (!$this->isHero()) {
            return false;
        }
        if (rand(1, 100) <= 10) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    private function hasMagicShield()
    {
        if (!$this->isHero()) {
            return false;
        }
        if (rand(1, 100) <= 20) {
            return true;
        }
        return false;
    }


    /**
     * @return bool
     */
    public function isHero()
    {
        return $this->hero;
    }

    /**
     * @return int
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * @param int $health
     */
    public function setHealth($health)
    {
        $this->health = $health;
    }

    /**
     * @return int
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * @param int $strength
     */
    public function setStrength($strength)
    {
        $this->strength = $strength;
    }

    /**
     * @return int
     */
    public function getDefence()
    {
        return $this->defence;
    }

    /**
     * @param int $defence
     */
    public function setDefence($defence)
    {
        $this->defence = $defence;
    }

    /**
     * @return int
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param int $speed
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }

    /**
     * @return int
     */
    public function getLuck()
    {
        return $this->luck;
    }

    /**
     * @param int $luck
     */
    public function setLuck($luck)
    {
        $this->luck = $luck;
    }

    /**
     * @return bool
     */
    public function getHero()
    {
        return $this->hero;
    }

    /**
     * @param bool $hero
     */
    public function setHero()
    {
        $this->hero = true;
    }

    /**
     * @return mixed
     */
    public function getPriorityAssignedBy()
    {
        return $this->priorityAssignedBy;
    }

    /**
     * @param mixed $priorityAssignedBy
     */
    public function setPriorityAssignedBy($priorityAssignedBy)
    {
        $this->priorityAssignedBy = $priorityAssignedBy;
    }




}